package ar.com.utn.ruleta.modelo;

import java.util.List;

import ar.com.utn.ruleta.modelo.exceptions.RuletaException;

public class Opcion2Numeros extends Opcion {

	public Opcion2Numeros(int saldo, Object valo) {
		super(saldo, valo);		
	}

	public void setValor(List<Numero> p2NumList) throws RuletaException{
		super.setValor(p2NumList);		
	}
	
	@Override
	public boolean validar(Numero pNum) {		
		return ((List)getValor()).contains(pNum);
	}

	@Override
	public int cobrar() {
		return getSaldo()*18;
	}

}
