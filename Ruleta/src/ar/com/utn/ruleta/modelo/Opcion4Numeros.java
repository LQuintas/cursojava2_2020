package ar.com.utn.ruleta.modelo;

import java.util.List;

import ar.com.utn.ruleta.modelo.exceptions.RuletaException;

public class Opcion4Numeros extends Opcion {

	public Opcion4Numeros(int saldo, Object valo) {
		super(saldo, valo);		
	}

	@Override
	public boolean validar(Numero pNum) {
		List<Numero> lista = (List<Numero>)getValor();		
		return lista.contains(pNum);
	}

	@Override
	public int cobrar() {
		return getSaldo()*9;
	}
	public void setNumero(List<Numero> pNumeros) throws RuletaException{
		//1-este metodo va a recibir 4 numeros
		//2-los numeros tienen que ser los abarque la ficha.
		//si esta todo bien
		// 0 1 2 3 posicion
 		// 1 2 4 5 valores
		boolean bln = false;
		bln = 	pNumeros.get(0).getValor()+1 == pNumeros.get(1).getValor() && // 1+1= 2
				pNumeros.get(2).getValor()+1 == pNumeros.get(3).getValor() && // 4+1= 5
				pNumeros.get(1).getValor()+2 == pNumeros.get(2).getValor() ;  // 2+2= 4  	
		if(bln)
			setValor(pNumeros);
		else
			throw new RuletaException("Lita de 4 numeros erronea");
	}

}
















