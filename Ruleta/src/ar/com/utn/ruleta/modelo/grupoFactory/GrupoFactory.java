package ar.com.utn.ruleta.modelo.grupoFactory;

import java.util.ArrayList;
import java.util.List;

import ar.com.utn.ruleta.modelo.Numero;

public abstract class GrupoFactory {
	private static List<GrupoFactory> grupos = new ArrayList<GrupoFactory>();
	
	
	public GrupoFactory() {
		super();
		grupos.add(new NegroFactory());
		grupos.add(new RojoFactory());
		grupos.add(new ImparFactory());
		grupos.add(new PrimerDocenaFactory());
		grupos.add(new SegundaDocena());
		grupos.add(new TercerDocena());
		grupos.add(new From19to36Factory());
		grupos.add(new From19to36Factory());
	}
	public static GrupoFactory getInstance(int pNumGrupo){
		for (GrupoFactory grupo : grupos) {
				if(grupo.isMe(pNumGrupo))
					return grupo;
		}
		return null;
	}
	public abstract boolean validarGrupo(Numero pNum);
	public abstract int cobrarGrupo();
	public abstract boolean isMe(int pNumGrupo);
}
