package ar.com.utn.ruleta.modelo;

import java.util.ArrayList;
import java.util.List;

import ar.com.utn.ruleta.modelo.exceptions.RuletaException;

/**
 *  Gabriel
 * Esta clase tiene la finalidad de alojar un numero
 * y todos los metod
 */
public class Numero {
	private int valor;
	//constructores
	public Numero(){
		valor=0;
	}
	public Numero(int pValor) throws RuletaException{
		setValor(pValor);		
	}
	/**
	 * Este metodo asigna un  valor al numero atraves del parametros  pValor 
	 * @param pValor
	 */
	public void setValor(int pValor) throws RuletaException{
		if(pValor<0 || pValor>36)
			throw new RuletaException();
		valor = pValor;
	}
	public int getValor(){
		return valor;
	}
	//metodos de negocio
	public boolean isRojo(){
		List<Integer> numerosRojos = new ArrayList<Integer>();
		//cargo del 1 al 9 lo impares
		for(int i=1;i<10; i= i + 2)
			numerosRojos.add(new Integer(i) );
		
		//cargo pares del 14 al 18
		for (int i=12;i<19; i +=2)
			numerosRojos.add(new Integer(i) );
		
		//desde el 19 al 27
		for (int i=19;i<28; i +=2)
			numerosRojos.add(new Integer(i) );
		//desde el 30 al 36

		for (int i=30;i<37; i +=2)
			numerosRojos.add(new Integer(i) );
		
		return numerosRojos.contains(new Integer(valor));
	}
	
	public boolean isNegro(){
		return !isRojo() &&
			   valor >0;
	}
	
	public boolean isPar(){
		return valor%2==0;
	}
	
	//falta
	public boolean isPrimeraDocena(){
		return false;
	}
	public boolean  isSegundaDocena(){
		return false;
	}
	public boolean isTercerDocena(){
		return false;
	}
	public boolean isPrimeraColumna(){
		return false;
	}
	public boolean isSegundaColumna(){
		return false;
	}
	public boolean isTercerColumna(){
		return false;
	}
	public boolean isFrom1To18(){
		return false;
	}
	public boolean isFrom19To36(){
		return false;
	}
	public boolean isCero(){
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	public boolean equals(Object obj){

		return obj instanceof Numero &&
				obj!=null 			 &&
			   ((Numero)obj).getValor() == valor;
	}
	public int hashCode(){
		return valor;
	}
    public String toString(){
    	return "valor=" + valor + "\n";
    }
	
	
	
	
	
	
	
	
	
	
}
