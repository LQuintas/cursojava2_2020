package ar.com.utn.ruleta.modelo;

import ar.com.utn.ruleta.modelo.exceptions.RuletaException;

public abstract class Opcion {
	//atributos
	private int 	saldo;
	private Object 	valor; //puede se un numero, una lista o un grupo
	
	public Opcion() {}
	
	//constructores
	public Opcion(int saldo, Object valo) {
		super();
		this.saldo = saldo;
		this.valor = valo;		
	}
	//getter y setter
	public int getSaldo() {		return saldo;	}
	public void setSaldo(int saldo) {		this.saldo = saldo;	}

	public Object getValor() {		return valor;	}
	public void setValor(Object valo) throws RuletaException {		this.valor = valo;	}
	
	
	//metodos de negocio
	public abstract boolean validar(Numero pNum);
	public abstract int cobrar();
	//equals haschcode y toString
	@Override
	public boolean equals(Object obj) {			
		return obj instanceof Opcion 					&&
				obj!=null								&&
				((Opcion)obj).getValor().equals(valor) 	&&
				((Opcion)obj).getSaldo()==saldo 		;
	}
	@Override
	public int hashCode() {		
		return valor.hashCode() + saldo;
	}
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("valor=");
		sb.append(valor);
		sb.append(",saldo=");
		sb.append(saldo);
		return sb.toString();
	}
	
	
	
	
	
	
	

}
