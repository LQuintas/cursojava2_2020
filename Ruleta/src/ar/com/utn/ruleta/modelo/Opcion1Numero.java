package ar.com.utn.ruleta.modelo;

import ar.com.utn.ruleta.modelo.exceptions.RuletaException;

public class Opcion1Numero extends Opcion {
	//constructor
	public Opcion1Numero(int saldo, Object valo) {
		super(saldo, valo);		
	}
	//setter
	public void setValor(Numero pNumero) throws RuletaException{
		super.setValor(pNumero);
	}
	//metodos de negocio
	public boolean validar(Numero pNumero) {		
		return pNumero.equals(getValor());
	}
	@Override
	public int cobrar() {		
		return getSaldo()*36;
	}
	
}









