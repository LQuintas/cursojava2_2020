package ar.com.utn.ruleta.modelo;

import ar.com.utn.ruleta.modelo.exceptions.RuletaException;

public class OpcionGrupo extends Opcion {
	
	public OpcionGrupo(int saldo, Object valor) throws RuletaException {
		super();
		setSaldo(saldo);
		setValor(valor);
		
	}

	public void setValor(Object valor) throws RuletaException {
		super.setValor(valor);
	}

	@Override
	public boolean validar(Numero pNum) {
		boolean bln=false;
		
		return bln;
	}

	@Override
	public int cobrar() {
		
		return getSaldo()*2;
	}

}
